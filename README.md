# Reproducable data science

Quarantine hack project.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/markgale%2Freproducable-data-science/HEAD?filepath=notebook.ipynb)

Playing with Jupyter notebooks on the Binder platform with some data in Python (Pandas and Seaborn)

![Jupyter logo](/docs/jupyter-logo.png "Jupyter")  ![Binder logo](/docs/binder-logo.png "Binder")

* [Jupyter](https://jupyter.org/) is a web-based interactive development environment for Jupyter notebooks, code, and data
* [Binder](https://mybinder.org/) is an online service for building and sharing reproducible and interactive computational environments from online repositories
* [Pandas](https://pandas.pydata.org/) is a Python data analysis library
* [Seaborn](https://seaborn.pydata.org/index.html) is a statistical data visualisation Python library
* The data itself:
  * [Bitcoin daily history](https://finance.yahoo.com/quote/BTC-USD/history) thanks to Yahoo Finance (care has been taken not to distribute this data)
  * [Twitter Happiness index](https://hedonometer.org/api.html) thanks to hedonometer project

Thanks to [The Turing Way](https://github.com/alan-turing-institute/the-turing-way/blob/master/workshops/boost-research-reproducibility-binder/workshop-presentations/zero-to-binder-python.md) for their workshop notes on zero-to-binder, which this was the foundations for this hack.


# Screenshot

![Screenshot](/docs/screenshot.png "Screenshot")
